import database from '../../src/database';

/* key specify a model that will b truncated */
export default function truncate(key) {
  return Promise.all(
    Object.keys(database.connection.models).map(() =>
    database.connection.models[key].destroy({
      truncate: true,
      force: true,
    })),
  );
};
