import request from 'supertest';
import bcrypt from 'bcryptjs'
import app from '../../src/app';

import User from '../../src/app/models/User'
import truncate from '../utils/truncate'

describe('Create User', ()=> {
  beforeEach(async ()=>{
    await truncate('User');
  })

  it('should be able to register', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('id', 'first_name', 'last_name', 'email');
  });

  it('should encrypt user password when new user is created', async ()=> {
    const user = await User.create({
        first_name: "Frederico",
        last_name: "Soares",
        email: "fred@gmail.com",
        password: "123456"
    });

    const compareHash = await bcrypt.compare('123456', user.password_hash);
    expect(compareHash).toBe(true);
  });

  it('should be able to return 400 if missing first name', async ()=> {
    const response = await request(app).post('/v1/users').send({
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      expect(response.status).toBe(400);
  });

  it('should be able to return 400 if missing last name', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      email: "fred@gmail.com",
      password: "123456"
      });

      expect(response.status).toBe(400);
  });

  it('should be able to return 400 if missing email', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      password: "123456"
      });

      expect(response.status).toBe(400);
  });

  it('should be able to return 400 if missing password', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      });

      expect(response.status).toBe(400);
  });

  it('should no be able to register duplicated email', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      const response = await request(app).post('/v1/users').send({
        first_name: "Frederico",
        last_name: "Soares",
        email: "fred@gmail.com",
        password: "123456"
        });

      expect(response.status).toBe(400);
  });
});


describe('Email validation', ()=> {

  it('should be able to return 400 if missing @', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fredgmail.com",
      password: "123456"
      });

      expect(response.status).toBe(400);
  });

  it('should be able to return 400 if missing dot', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fredgmail.com",
      password: "123456"
      });

      expect(response.status).toBe(400);
  });

  it('should be able register if email is correct', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      expect(response.status).toBe(400);
  });

});


describe('Password validation', ()=> {

  it('should be able to return 400 if password is less than 6 characters', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "12345"
      });

      expect(response.status).toBe(400);
  });

  it('should be able register if password is correct', async ()=> {
    const response = await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      expect(response.status).toBe(400);
  });

});


describe('User session', ()=> {
  beforeEach(async ()=>{
    await truncate('User');
  })

  it('should be able to login', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      const response = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('token');
  });

  it('should not be able to login if password is wrong', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      const response = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "1234444"
        });

      expect(response.status).toBe(401);
  });

  it("should not be able to login if user doesn't exists", async ()=> {
      const response = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      expect(response.status).toBe(401);
  });

});

describe('Update User', ()=> {
  beforeEach(async ()=>{
    await truncate('User');
  })

  it('should be able to update', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      const session = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      const response = await request(app).patch('/v1/users')
      .set('Authorization', `Bearer ${session.body.token}`).send({
          first_name: "Frederico",
          last_name: "Soares",
          email: "fre@gmail.com",
        });

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('token');
  });

  it('should not be able to update with duplicate email', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      await request(app).post('/v1/users').send({
        first_name: "Fred",
        last_name: "Silva",
        email: "fs@gmail.com",
        password: "123456"
        });

      const session = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      const response = await request(app).patch('/v1/users')
      .set('Authorization', `Bearer ${session.body.token}`).send({
          first_name: "Frederico",
          last_name: "Soares",
          email: "fs@gmail.com",
        });

      expect(response.status).toBe(401);
  });

  it('should not be able to update with wrong password', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      const session = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      const response = await request(app).patch('/v1/users')
      .set('Authorization', `Bearer ${session.body.token}`).send({
          password: "123456",
          oldPassword: "12345678",
          confirmPassword: "123456"
        });

      expect(response.status).toBe(401);
  });

});

describe('List all Users', ()=> {
  beforeEach(async ()=>{
    await truncate('User');
  })

  it('should be able to list all users', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      await request(app).post('/v1/users').send({
        first_name: "Fred",
        last_name: "Soares",
        email: "fred2@gmail.com",
        password: "1234567"
        });

      const session = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      const response = await request(app).get('/v1/users')
      .set('Authorization', `Bearer ${session.body.token}`).send();

      expect(response.status).toBe(200);
  });

  it('should not be able to list users for user that is not authenticated', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred4@gmail.com",
      password: "123456"
      });

      const session = await request(app).post('/v1/session').send({
        email: "fred4@gmail.com",
        password: "123456"
        });

      const response = await request(app).patch('/v1/users').send();

      expect(response.status).toBe(401);
  });
});

describe('Delete User', ()=> {
  beforeEach(async ()=>{
    await truncate('User');
  })

  it('should be able to delete user', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      await request(app).post('/v1/users').send({
        first_name: "Fred",
        last_name: "Soares",
        email: "fred2@gmail.com",
        password: "1234567"
        });

      const session = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      const response = await request(app).delete('/v1/users')
      .set('Authorization', `Bearer ${session.body.token}`).send();

      expect(response.status).toBe(200);
  });

  it('should not be able to delete a user when user is not authenticated', async ()=> {
    await request(app).post('/v1/users').send({
      first_name: "Frederico",
      last_name: "Soares",
      email: "fred@gmail.com",
      password: "123456"
      });

      const session = await request(app).post('/v1/session').send({
        email: "fred@gmail.com",
        password: "123456"
        });

      const response = await request(app).delete('/v1/users').send();

      expect(response.status).toBe(401);
  });
});
