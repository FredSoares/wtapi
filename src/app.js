import './bootstrap'

import express from 'express';
import routes from './routes'
const cors = require('cors');

import './database';

class App {
  constructor() {
    this.server = express();

    this.middlewares();
    this.routes();
  }

  middlewares(){
    /* middleware that allow us to control witch aplications can access the api */
    this.server.use(cors());
    /* configuration to allow express to receive request in JSON format */
    this.server.use(express.json());
  }

  routes(){
    this.server.use(routes);
  }
}

export default new App().server;
