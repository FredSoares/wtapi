import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';
import uuid from 'uuid';


class User extends Model {
  static init(sequelize) {
    super.init({
      first_name: Sequelize.STRING,
      last_name: Sequelize.STRING,
      email: Sequelize.STRING,
      token: Sequelize.STRING,
      password: Sequelize.VIRTUAL, /* virtual field, will not be saved in the DB */
      password_hash: Sequelize.STRING,
      user_type_id: {
        type: Sequelize.STRING,
        defaultValue: 1,
      },
    },
    {
      tableName: 'users',
      underscored: true,
      sequelize,
    });

    /* hook that will executed before saved to DB */
    this.addHook('beforeSave', async (user) => {
      /* verifiy if the password field exists */
      if (user.password) {
        /* do password encryption with weight 8 */
        user.password_hash = await bcrypt.hash(user.password, 8);
        /* create token */
        user.token = uuid.v4();
      }
    });

    return this;
  }

  /* receive all models and set up associations */
  static associate(models) {
    this.belongsTo(models.UserType, { foreignKey: 'user_type_id', as: 'userType' });
  }

  /* password validation */
  checkPassword(password) {
    /* compare passwords */
    return bcrypt.compare(password, this.password_hash);
  }
}

export default User;
