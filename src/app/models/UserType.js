import Sequelize, { Model } from 'sequelize';

class UserType extends Model {
  static init(sequelize) {
    super.init({
      name: Sequelize.STRING,
    },
    {
      tableName: 'user_types',
      underscored: true,
      sequelize,
    });

    return this;
  }
}

export default UserType;
