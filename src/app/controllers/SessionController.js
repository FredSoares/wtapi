import User from '../models/User';

class SessionController {
  async store(req, res) {
    /* get email and password in the body of the request */
    const { email, password } = req.body;

    /* check if user exist */
    const user = await User.findOne({
      where: { email },
    });

    /* if not exists return error message */
    if (!user) {
      return res.status(401).json({ error: 'User not found' });
    }
    /* check if the password is correct */
    if (!(await user.checkPassword(password))) {
      return res.status(401).json({ error: 'Password does not match' });
    }

    /* if all is correct return user data and the token */
    const {
      id, first_name, last_name, token, user_type_id
    } = user;

    return res.json({
      user:{
        id,
        first_name,
        last_name,
        email,
        user_type_id,
      },
      token,
    });
  }
}

module.exports = new SessionController();
