import User from '../models/User';
import UserType from '../models/UserType';

class UserController {
  /* function to create user */
  async store(req, res) {
    try {
      /* check if email already exists */
      const userExists = await User.findOne({
        where: {
          email: req.body.email,
        },
      });
      /* if email already exists return error */
      if (userExists) {
        return res.status(400).json({ error: 'User already exists.' });
      }
      /* store user */
      const {
        id, first_name, last_name, email, user_type_id
      } = await User.create(req.body);

      return res.json( {
        id, first_name, last_name, email, user_type_id
      });
    } catch (error) {
      return res.status(500);
    }
  }

  /* function to update user */
  async update(req, res) {
    try {
      /* get email and oldPassword from request body */
      const { email, oldPassword, } = req.body;
      /* select user from ID */
      const user = await User.findByPk(req.userId);
      if (!user) {
        return res.status(404).json({ error: 'User not found.' });
      }

      /* Verify that the email is different from the request email */
      if (email && email !== user.email) {
        /* verify if the new email is already exist */
          const userExists = await User.findOne({ where: { email } });
          /* if already exist return error message */
          if (userExists) {
            return res.status(401).json({ error: 'Email already exists' });
          }
      }

      /* if the email does not already exist, validate the password
        check that the old password has been sent and is correct */
      if (oldPassword && !(await user.checkPassword(oldPassword))) {
        console.error('Password does not match');
        return res.status(401).json({ error: 'Password does not match' });
      }

      /* update user */
        const {
          id, first_name, last_name, token, user_type_id
        } = await user.update(req.body);

      return res.json({
        user:{
          id,
          user_type_id,
          first_name,
          last_name,
          email,
          user_type_id,
        },
        token,
      });

    } catch (error) {
      return res.status(500);
    }
  }

  /* function to get all users */
  async index(req, res) {

    try{
      /* pagination, start in 1 */
      const { page = 1 } = req.query;
      /* limit post */
      const limit = 20;
      /* select all user post from table post */

      const users = await User.findAll({

        attributes: ['id', 'first_name', 'last_name', 'email', 'created_at'],
        order: [['first_name', 'ASC']],
        limit,
        offset: (page - 1) * limit,
        include: [
          {
            model: UserType,
            as: 'userType',
            attributes: ['name'],
          },
        ],
      });
      return res.json(users);

    } catch (error) {
      return res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  /* function to delete user */
  async delete(req, res) {
    try {
      /* select user from ID */
      const user = await User.findByPk(req.userId);
      if (!user) {
        return res.status(404).json({ error: 'User not found.' });
      }

      /* delete user */
      await User.destroy({
        where: {
          id: req.userId,
        },
      });

      return res.status(200).json({ message: 'User deleted' });

    } catch (error) {
      return res.status(500);
    }
  }
}

export default new UserController();
