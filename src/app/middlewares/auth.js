import User from '../models/User';

export default async (req, res, next) => {
  /* get the authentication token in the request header */
  const authHeader = req.headers.authorization;

  /* check if the token was sent */
  if (!authHeader) {
    return res.status(401).json({ error: 'Token not provided' });
  }

  /* get the token using destructuring because array is in the following format [Bearer, token] */
  const [, token] = authHeader.split(' ');
  try {
    /* check if any user have the token and get user id */
    const { id, user_type_id } = await User.findOne({
      where: {
        token,
      },
    });

    /* if not found return error */
    if (!id) {
      return res.status(401).json({ error: 'Token invalid' });
    }
    /* put user id in the request */
    req.userId = id;
    req.userType = user_type_id;
    return next();
  } catch (error) {
    /* if token invalid return error message */
    return res.status(401).json({ error: 'Token invalid' });
  }
};
