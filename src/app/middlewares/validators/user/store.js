import * as Yup from 'yup';

export default async (req, res, next) => {
  try {
    /* data schema */
    const schema = Yup.object().shape(
      {
        first_name: Yup.string().required(),
        last_name: Yup.string().required(),
        email: Yup.string().email().required(),
        password: Yup.string().required().min(6),
      },
    );

    /* validate schema */
    await schema.validate(req.body, { abortEarly: false });
    return next();
  } catch (error) {
    return res.status(400).json({ error: 'Validation fails', messages: error.inner });
  }
};
