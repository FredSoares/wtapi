import Sequelize from 'sequelize';

import databaseConfig from '../config/database';

/* models imports */
import User from '../app/models/User';
import UserType from '../app/models/UserType';

/* array with all models */
const models = [User, UserType];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    /* initialize all models with the connection configuration */
    models
      .map((model) => model.init(this.connection))
      /* for each array element call the assocition method by passing the connection */
      .map((model) => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
