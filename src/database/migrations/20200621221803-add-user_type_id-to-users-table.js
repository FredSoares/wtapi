'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    /* table to insert */
    'users',
    /* column to insert */
    'user_type_id',
    {
      type: Sequelize.INTEGER,
      references: { model: 'user_types', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'NO ACTION',
      allowNull: true,
    },
  ),

  down: (queryInterface) => queryInterface.removeColumn('users', 'user_type_id'),
};
