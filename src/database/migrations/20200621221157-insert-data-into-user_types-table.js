'use strict';

module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert('user_types', [
    {
      name: 'administrator',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      name: 'collaborator',
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]),

  down: (queryInterface) => queryInterface.bulkDelete('user_types', null, {}),
};
