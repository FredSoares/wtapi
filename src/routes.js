import { Router } from 'express';

import UserController from './app/controllers/UserController'
import SessionController from './app/controllers/SessionController'

/* import authentication middleware */
import authMiddleware from './app/middlewares/auth';

/* validators middlewares imports */
import ValidateUserStore from './app/middlewares/validators/user/store';
import ValidateUserUpdate from './app/middlewares/validators/user/update';

const routes = new Router()


/**
 * @route   POST api/session
 * @desc    Create new session
 * @access  Public
 */
routes.post('/v1/session', SessionController.store)

/**
 * @route   POST api/users
 * @desc    Register new user
 * @access  Public
 */
routes.post('/v1/users', ValidateUserStore, UserController.store)

/* defines as global middleware.
 * will only take effect for the routes below as it has been declared
 * after the routes above
 * */
routes.use(authMiddleware);

/**
 * @route   PATCH api/users
 * @desc    Update user
 * @access  Private
 */
routes.patch('/v1/users', ValidateUserUpdate, UserController.update)

/**
 * @route   GET api/users
 * @desc    List all Users
 * @access  Private
 */
routes.get('/v1/users', UserController.index)

/**
 * @route   DELETE api/users
 * @desc    Delete a user
 * @access  Private
 */
routes.delete('/v1/users', UserController.delete)

export default routes;
